const express = require("express");
const app = express();
const port = 3000;

app.get("/", (req, res) => {
  res.send("Merci d'appeler /allo");
});

app.get("/allo", (req, res) => {
  const currentDate = new Date().toISOString();
  const myMessage = "Moshi Moshi";
  let htmlPage = `
<!DOCTYPE html>

<html lang="en">

    <meta charset="UTF-8">
    
    <title>Allo SSR App</title>

    <body>

        <h2>
            ${myMessage}
        </h2>
        
        <h1>
            ${currentDate}
        </h1>
    
    </body>

</html>
`;
  res.send(htmlPage);
});

const addr = "localhost";
// const addr = "127.0.0.1";
// const addr = "0.0.0.0";
app.listen(port, addr, () => {
  console.log(`Allo SSR app écoute sur l'adresse ${addr} et le port ${port}`);
});
